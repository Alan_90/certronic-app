'use strict';

angular.module('myApp.services', [])
    .config(function () {});

'use strict';

angular.module('myApp.services').service('ApiHttpSrv', function ($http) {

    // returns a timestamp for the requests
    var getTimestamp = function(){
        return new Date().getTime();
    };



    //creates an http object with required headers
    var createHttp = function(type, url, data, headers){
        var config = {
            method: type,
            url: url,
            data: data ? data : '',
            cache: false,
            headers: (headers) ? headers : []
        };
        // create http
        return $http(config);
    };

    //creates an http resource, from an api endpoint baseurl
    //Use name and map it with configSrv
    var createApiHttp = function(type, baseurl, data, params, headers){
        // convert params object into a string of params, if not string
        if(params) {
            params = angular.isString(params) ? params : $.param(params);
        }
        // create url
        var url = baseurl + (params ? '?'+params : '');
        // create http
        return createHttp(type, url, data, headers);
    };

    //Response status form 200 to 299 are considered successful responses
    var isSuccessResponse = function(response) {
        return (response.status && response.status >= 200 && response.status <= 299);
    };

    //return service
    return {
        createHttp: createHttp,
        createApiHttp: createApiHttp,
        getTimestamp: getTimestamp,
        isSuccessResponse: isSuccessResponse
    };
});


'use strict';

angular.module('myApp.services').service('AuthSrv', function ($http, $rootScope, ConfigSrv, ApiHttpSrv, RedirectSrv) {
    var currentUser = null, auth = false, initialState = true;


    var initialState = function () {
        return !angular.isDefined(localStorage.getItem('user')) && initialState;
    };
    var login = function (data) {
        var loginSuccess = function(d){

            if(d[0].status === 'ok'){
                currentUser = {
                    type : d[0].user_type,
                    token : d[0].token,
                    idEmpresa : d[0].idEmpresa
                };
                auth = true;
                localStorage.setItem("user", data.user);
                localStorage.setItem("type", currentUser.type);
                localStorage.setItem("token", currentUser.token);
                localStorage.setItem("idEmpresa", currentUser.idEmpresa);
                initialState = false;
                $rootScope.loading = false;
                $rootScope.updateHome = true; // inicialmente debo obtener data de la home
                if(currentUser.type === 'contratista'){
                    RedirectSrv.redirect('/');
                }
                if(currentUser.type === 'planta'){
                    RedirectSrv.redirect('/homePlanta');
                }
                
            }else{
                auth = false;
                $rootScope.msg = d[0].msg;
                $rootScope.loginfail = true;
                $rootScope.loading = false;
            }
            return auth;
        };
        var loginFail = function(data){
            console.log(data);
            $rootScope.disconnect = true;
            $rootScope.loading = false;
        };
        $rootScope.loading = true;
        ApiHttpSrv.createApiHttp('post', ConfigSrv.getApiUrl('login'), data, data).success(loginSuccess).error(loginFail);

    };

    var logout = function () {
        //var logoutSuccess = function(data){
            currentUser = null;
            auth = false;
            initialState = true;
            localStorage.removeItem("user");
            localStorage.removeItem("type");
            localStorage.removeItem("token");
            localStorage.removeItem("idEmpresa");
        //};
        //ApiHttpSrv.createApiHttp('get', ConfigSrv.getApiUrl('logout'), []).success(logoutSuccess);
    };
    var isLoggedIn = function () {
        return auth;
    };
    var currentUser = function () {
        return currentUser;
    };
    var authorized = function () {
        if(!auth){
            if(angular.isDefined(localStorage.getItem('user')) && localStorage.getItem('user') != null){
                currentUser = {
                    user : localStorage.getItem('user'),
                    type : localStorage.getItem('type'),
                    token : localStorage.getItem('token'),
                    idEmpresa : localStorage.getItem('idEmpresa')
                };
                auth = true;
            }else{
                return false;
            }
        }
        return auth;
    };

    //return service
    return {
        initialState: initialState,
        login: login,
        logout: logout,
        isLoggedIn: isLoggedIn,
        currentUser: currentUser,
        authorized: authorized
    };
});
'use strict';

angular.module('myApp.services').service('ConfigSrv', function (config) {
    //assert helper for config service
    var _assert = function(expression, msg){
        if(!expression) {
            throw 'ConfigSrv ERROR: ' + msg;
        }
    };

    //get config by json path, for example getConfig('x.y.z')
    var getConfig = function(path){
        return _keypath(config,path);
    };

    // get api url by method name
    var getApiUrl = function(method_name) {
        //get Method Config
        var methodConf = getConfig('api.methods.'+method_name);
        _assert(methodConf, 'api method '+method_name+' is not defined in api.methods');

        return methodConf;
    };

    // get a list of routes configurations, outside function for performance
    var routeConfig = getConfig('routes');

    // returns the config route
    var getRouteConfig = function getRouteConfig(urlpath) {
        var result =  _.find(routeConfig, function(conf){
            var re = new RegExp('^'+conf.path+'$');
            var matches = re.test(urlpath);
            return matches;
        });
        return result;
    };

    //return service
    return {
        getConfig: getConfig,
        getApiUrl: getApiUrl,
        getRouteConfig: getRouteConfig
    };
});

//TODO move
window._keypath = function(content, key){
    var originalKeyString, keys, partialContent;

    if( !_.isString(key) || _.isEmpty(content)){
        return;
    }

    if( key.indexOf('.') === -1 ){
        return content[key] || undefined;
    }else{
        partialContent = content;
        originalKeyString = key;
        keys = key.split('.');

        keys.forEach(function(key){
            if( !partialContent ){
                return;
            }

            partialContent = partialContent[key] || undefined;
        });

        return partialContent;
    }
};
'use strict';

angular.module('myApp.services').service('RedirectSrv', function ($location){

	var redirectTo = function (hash){
		$location.url(hash);
	};



	return{
		redirect: redirectTo
	};

});
'use strict';

/* Filters */

filters.filter('interpolate', ['version', function (version) {
    return function (text) {
        return String(text).replace(/\%VERSION\%/mg, version);
    }
}]);

'use strict';

directives.directive('fadeIn', function () {
    return {
        compile:function (elm) {
            $(elm).css('opacity', 0.0);
            return function (scope, elm, attrs) {
                $(elm).animate({ opacity:1.0 }, 1000);
            };
        }
    };
});
'use strict';

// this is the angular way to stop even propagation
directives.directive('stopEvent', function () {
    return {
        restrict:'A',
        link:function (scope, element, attr) {
            element.bind(attr.stopEvent, function (e) {
                e.stopPropagation();
            });
        }
    }
});
'use strict';

angular.module('myApp.main', [])
    .controller('mainCtrl',
        ['$scope', '$rootScope', '$window', 'ApiHttpSrv', 'ConfigSrv', '$location', 'AuthSrv', 'RedirectSrv',
        function($scope, $rootScope, $window, ApiHttpSrv, ConfigSrv, $location, AuthSrv, RedirectSrv) {

    // Evento Apache Cordova / Phonegap
    document.addEventListener("deviceready", onDeviceReady, false);
    //lo que carga al actualizar la pagina
    if (AuthSrv.initialState() || !AuthSrv.authorized()) {
        $location.path("/login");
    }else{
        $rootScope.updateHome = true; // inicialmente debo actualizar la home
        if(AuthSrv.currentUser().type === 'contratista'){
            RedirectSrv.redirect('/');
        }else{
            RedirectSrv.redirect('/homePlanta');
        }
    }

    $rootScope.logout = function () {
        AuthSrv.logout();
        $rootScope.data = null;
        $rootScope.plantas = null;
        $rootScope.currentId = null;
        $rootScope.plant = null;
        $rootScope.collections = {};
    }
    $rootScope.changePlant = function(id){
        $rootScope.currentId = id;
        $rootScope.collections = {}; // destruyo las colecciones (para prevenir errores con back button)
        switch(AuthSrv.currentUser().type){
            case 'contratista':
                $rootScope.plant = $rootScope.plantas[id];
                RedirectSrv.redirect('/');
                break;
            case 'planta':
                $rootScope.data = $rootScope.plantas[id];
                RedirectSrv.redirect('/homePlanta');
                break;
        }
    }

    $scope.goToContratos = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/contratos');
                break;
            case 'planta':
                RedirectSrv.redirect('/contratosPlanta');
                break;
        }
    }

    $scope.goToContratoPage = function(id){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/contrato/' + $rootScope.plant.id + '/' + id);
            break;
            case 'planta':
                RedirectSrv.redirect('/contratoPlanta/' + id);
                break;
        }
    }

    $scope.goToContratistas = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/contratistas');
                break;
            case 'planta':
                RedirectSrv.redirect('/contratistasPlanta');
                break;
        }
    }

    $scope.goToContratistaPage = function(id){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/contratista/' + $rootScope.plant.id + '/' + id);
                break;
            case 'planta':
                RedirectSrv.redirect('/contratistaPlanta/' + id);
                break;
        }
    }

    $scope.goToPersonal = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/personal');
                break;
            case 'planta':
                RedirectSrv.redirect('/personalPlanta');
                break;
        }
        
    }

    $scope.goToEmpleadoPage = function(id){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/empleado/' + $rootScope.plant.id + '/' + id);
                break;
            case 'planta':
                RedirectSrv.redirect('/empleadoPlanta/' + id);
                break;
        }
    }

    $scope.goToMaquinarias = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/maquinarias');
                break;
            case 'planta':
                RedirectSrv.redirect('/maquinariasPlanta');
                break;
        }
    }

    $scope.goToMaquinariaPage = function(id){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/maquinaria/' + $rootScope.plant.id + '/' + id);
                break;
            case 'planta':
                RedirectSrv.redirect('/maquinariaPlanta/' + id);
                break;
        }
    }

    $scope.goToVehiculos = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/vehiculos');
                break;
            case 'planta':
                RedirectSrv.redirect('/vehiculosPlanta');
                break;
        }
    }

    $scope.goToVehiculoPage = function(id){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/vehiculo/' + $rootScope.plant.id + '/' + id);
                break;
            case 'planta':
                RedirectSrv.redirect('/vehiculoPlanta/' + id);
                break;
        }
    }

    $scope.goToReportes = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/reportes');
                break;
            case 'planta':
                RedirectSrv.redirect('/reportesPlanta');
                break;
        }
    }

    $scope.goToExcepciones = function(){
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
            //    RedirectSrv.redirect('/vehiculos');
                break;
            case 'planta':
                RedirectSrv.redirect('/excepcionesPlanta');
                break;
        }
    }

    $scope.goToExcepcionesPage = function(id){
        console.log(id)
        RedirectSrv.redirect('/excepcionPlanta/' + id);
        
    }

    

    $rootScope.goBack = function(){
        $window.history.back();
    }

    $rootScope.goToHome = function(){
        // $rootScope.updateHome = true;  // si aprieto inicio actualiza la home
        switch(AuthSrv.currentUser().type) {
            case 'contratista':
                RedirectSrv.redirect('/');
                break;
            case 'planta':
                RedirectSrv.redirect('/homePlanta');
                break;
        }
    }

    $rootScope.open = function(link){
        $window.open(link, '_blank');
    }

    $scope.collapse = function(id){
        $('#' + id).removeClass('in');
        $('#' + id).addClass('collapse');
    }

    // Evento Apache Cordova / Phonegap
    function onDeviceReady() {
        $scope.$apply(function() {
            document.addEventListener("backbutton", onBackKeyButton, false); //Listen to the User clicking on the back button
        });
    }

    function onBackKeyButton(evt) {
        evt.preventDefault();
        if ($location.path() == '/' || $location.path() == '/login' || $location.path()== '/homePlanta') {
            navigator.notification.confirm(
                '¿Desea cerrar la aplicación?', // message
                 exitApp,            // callback to invoke with index of button pressed
                'SALIR',           // title
                ['NO','SÍ']     // buttonLabels
            );
        }
        else {
            window.history.back();
        }
    }

    function exitApp(buttonIndex) {
        if(buttonIndex == 2) {
            navigator.app.exitApp();
        }
    }

}]);
/**
 * Controller para listados: contratos, contratistas, personal, vehículos, maquinarias
 */
'use strict';

angular.module('myApp.collections', [])
    .controller('collectionsCtrl',
        ['$scope', '$rootScope','context', '$window', '$filter', 'AuthSrv', 'RedirectSrv', 'ApiHttpSrv', 'ConfigSrv',
        function($scope, $rootScope, context, $window, $filter, AuthSrv,  RedirectSrv, ApiHttpSrv, ConfigSrv) {

        if (AuthSrv.initialState() || !AuthSrv.authorized()) {
            RedirectSrv.redirect('/login');
        };
        if(!$rootScope.plant) {
            $rootScope.goToHome();
        }

        // los items son guardados en $rootScope.collections.<type> cuando se obtiene la data
        $scope.pagedItems = [];
        $scope.currentPage = 0;

        var itemsPerPage = 100;
        var filteredItems = [];
        var sortingOrder = context.sortingOrder;
        var reverse = false;

        var url_get = ConfigSrv.getApiUrl(context.type); // en context.type se setea el tipo de coleccion (contratos, vehiculos, etc)

        var getData = function() {
            var data = {
                token : AuthSrv.currentUser().token,
                idPlanta : $rootScope.plant.id,
                idEmpresa : $rootScope.plant.idEmpresa,
                idContratista : $rootScope.plant.idContratista  
            }
            $scope.loading = true;
            ApiHttpSrv.createApiHttp('get', url_get, data, data)
            .success(function(d){
                $rootScope.collections[context.type] = d;
                groupToPages();
                // $scope.search();
                $scope.loading = false;
                $rootScope.updateCollection = false; // flag de actualizar en false: no se volverá a pedir la data hasta no setearlo en true
                $scope.currentPlantId = $rootScope.plant.id
            }).error(function(d){
                $scope.loading = false;
            });
        }

        var searchMatch = function (haystack, needle) {
            if (!needle) {
                return true;
            }
            if(haystack){
                return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
            }
        };

        // calculate page in place
        var groupToPages = function (filtered) {
            $scope.pagedItems = [];

            var items = $rootScope.collections[context.type];
            if (filtered) {
                items = $scope.filteredItems;
            }

            for (var i = 0; i < items.length; i++) {
                if (i % itemsPerPage === 0) {
                    $scope.pagedItems[Math.floor(i / itemsPerPage)] = [ items[i] ];
                } else {
                    $scope.pagedItems[Math.floor(i / itemsPerPage)].push(items[i]);
                }
            }
        };

        // init the filtered items
        $scope.search = function () {
            $scope.filteredItems = $filter('filter')($rootScope.collections[context.type], function (item) {
                for(var attr in item) {
                    if (searchMatch(item[attr], $scope.query))  // si no hay query retorna true
                        return true;
                }
                return false;
            });
            // take care of the sorting order
            if (sortingOrder && sortingOrder !== '') {
                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, sortingOrder, $scope.reverse);
            }
            $scope.currentPage = 0;
            // now group by pages
            groupToPages(true);
        };

        // Cargo y muestro data:
        if (!$rootScope.collections[context.type]) {
            // si no hay items, obtengo la data
            getData();
        }
        else {
            groupToPages();
        }

        $scope.range = function (start, end) {
            var ret = [];
            if (!end) {
                end = start;
                start = 0;
            }
            for (var i = start; i < end; i++) {
                ret.push(i);
            }
            return ret;
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pagedItems.length - 1) {
                $scope.currentPage++;
            }
        };

        $scope.setPage = function () {
            $scope.currentPage = this.n;
        };

        // change sorting order
        $scope.sort_by = function(newSortingOrder) {
            if (sortingOrder == newSortingOrder)
                reverse = !reverse;

            sortingOrder = newSortingOrder;
        };

}]);


/**
 * Controller para listados: contratos, contratistas, personal, vehículos, maquinarias de la planta
 */
'use strict';

angular.module('myApp.collectionsPlanta', [])
    .controller('collectionsPlantaCtrl',
        ['$scope', '$rootScope','context', '$window', '$filter', 'AuthSrv', 'RedirectSrv', 'ApiHttpSrv', 'ConfigSrv',
        function($scope, $rootScope, context, $window, $filter, AuthSrv,  RedirectSrv, ApiHttpSrv, ConfigSrv) {

        if (AuthSrv.initialState() || !AuthSrv.authorized()) {
            RedirectSrv.redirect('/login');
        };
        if(!$rootScope.data) {
            $rootScope.goToHome();
        }

        // los items son guardados en $rootScope.collections.<type> cuando se obtiene la data
        $scope.pagedItems = [];
        $scope.currentPage = 0;

        var itemsPerPage = 100;
        var filteredItems = [];
        var sortingOrder = context.sortingOrder;
        var reverse = false;

        var url_get = ConfigSrv.getApiUrl(context.type); // en context.type se setea el tipo de coleccion (contratos, vehiculos, etc)

        var getData = function() {
            var data = {
                token : AuthSrv.currentUser().token,
                idEmpresa : AuthSrv.currentUser().idEmpresa,
                idPlanta : $rootScope.data.idPlanta
            }
            $scope.loading = true;
            ApiHttpSrv.createApiHttp('get', url_get, data, data)
            .success(function(d){
                $rootScope.collections[context.type] = d;
                groupToPages();
                // $scope.search();
                $scope.loading = false;
                $rootScope.updateCollection = false // flag de actualizar en false: no se volverá a pedir la data hasta no setearlo en true
            }).error(function(d){
                $scope.loading = false;
            });
        }

        var searchMatch = function (haystack, needle) {
            if (!needle) {
                return true;
            }
            if(haystack){
                return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
            }
        };

        // calculate page in place
        var groupToPages = function (filtered) {
            $scope.pagedItems = [];

            var items = $rootScope.collections[context.type];
            if (filtered) {
                items = $scope.filteredItems;
            }

            for (var i = 0; i < items.length; i++) {
                if (i % itemsPerPage === 0) {
                    $scope.pagedItems[Math.floor(i / itemsPerPage)] = [ items[i] ];
                } else {
                    $scope.pagedItems[Math.floor(i / itemsPerPage)].push(items[i]);
                }
            }
        };

        // init the filtered items
        $scope.search = function () {
            $scope.filteredItems = $filter('filter')($rootScope.collections[context.type], function (item) {
                for(var attr in item) {
                    if (searchMatch(item[attr], $scope.query))  // si no hay query retorna true
                        return true;
                }
                return false;
            });
            // take care of the sorting order
            if (sortingOrder && sortingOrder !== '') {
                $scope.filteredItems = $filter('orderBy')($scope.filteredItems, sortingOrder, $scope.reverse);
            }
            $scope.currentPage = 0;
            // now group by pages
            groupToPages(true);
        };

        // Cargo y muestro data:
        if (!$rootScope.collections[context.type]) {
            // si no hay items, obtengo la data
            getData();
        }
        else {
            groupToPages();
        }

        $scope.range = function (start, end) {
            var ret = [];
            if (!end) {
                end = start;
                start = 0;
            }
            for (var i = start; i < end; i++) {
                ret.push(i);
            }
            return ret;
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pagedItems.length - 1) {
                $scope.currentPage++;
            }
        };

        $scope.setPage = function () {
            $scope.currentPage = this.n;
        };

        // change sorting order
        $scope.sort_by = function(newSortingOrder) {
            if (sortingOrder == newSortingOrder)
                reverse = !reverse;

            sortingOrder = newSortingOrder;
        };
}]);



/**
 * Controller para mostrar la data de una entidad: contrato, contratista, empleado, vehículo, maquinaria
 */
'use strict';

angular.module('myApp.entity', [])
.controller('entityCtrl',
    ['$scope', '$rootScope','$routeParams', 'context', '$window', 'AuthSrv', 'RedirectSrv', 'ApiHttpSrv', 'ConfigSrv',
    function($scope, $rootScope, $routeParams, context, $window, AuthSrv, RedirectSrv, ApiHttpSrv, ConfigSrv) {

    var url_get = ConfigSrv.getApiUrl(context.type); // en context.type se setea el tipo de entidad (contratos, vehiculos, etc)

    var getData = function () {
        var data = {
            'token': AuthSrv.currentUser().token,
            'idPlanta': $routeParams.idP,
            'id': $routeParams.id,
            'idEmpresa' : AuthSrv.currentUser().idEmpresa 
        };
        var getDataSuccess = function(d){
            $scope.entityData = d[0];
            $scope.loading = false;
        };
        var getDataFail = function(d){
            $scope.loading = false;
        };
        $scope.loading = true;
        ApiHttpSrv.createApiHttp('post', url_get, data, data).success(getDataSuccess).error(getDataFail);
    }

    if (AuthSrv.initialState() || !AuthSrv.authorized()) {
        RedirectSrv.redirect('/login');
    }else{
        getData();
    }

}]);

/**
 * Controller para mostrar la data de una entidad: contrato, contratista, empleado, vehículo, maquinaria en la planta
 */
'use strict';

angular.module('myApp.entityPlanta', [])
.controller('entityPlantaCtrl',
    ['$scope', '$rootScope','$routeParams', 'context', '$window', 'AuthSrv', 'RedirectSrv', 'ApiHttpSrv', 'ConfigSrv',
    function($scope, $rootScope, $routeParams, context, $window, AuthSrv, RedirectSrv, ApiHttpSrv, ConfigSrv) {

    var url_get = ConfigSrv.getApiUrl(context.type); // en context.type se setea el tipo de entidad (contratos, vehiculos, etc)
    var getData = function () {
        var data = {
            'token': AuthSrv.currentUser().token,
            'id': $routeParams.id,
            'idEmpresa' : AuthSrv.currentUser().idEmpresa,
            'idPlanta' : $rootScope.data.idPlanta
        };
        var getDataSuccess = function(d){
            $scope.entityData = d[0];
            $scope.loading = false;
        };
        var getDataFail = function(d){
            $scope.loading = false;
        };
        $scope.loading = true;
        ApiHttpSrv.createApiHttp('post', url_get, data, data).success(getDataSuccess).error(getDataFail);
    }

    if (AuthSrv.initialState() || !AuthSrv.authorized()) {
        RedirectSrv.redirect('/login');
    }else{
        getData();
    }

}]);

'use strict';

angular.module('myApp.home', [])
.controller('homeCtrl',
    ['$scope', '$rootScope', 'ApiHttpSrv', 'ConfigSrv', '$location', 'AuthSrv', 'RedirectSrv',
    function($scope, $rootScope, ApiHttpSrv, ConfigSrv, $location, AuthSrv, RedirectSrv) {

        var getdata = function () {
            var data = {
                'token': AuthSrv.currentUser().token,
                'idEmpresa': AuthSrv.currentUser().idEmpresa
            };
            var getDataSuccess = function(data){
                // console.log(data[0]);
                $rootScope.data = data[0];
                $rootScope.plantas = $rootScope.data.plantas;

                if(!$rootScope.currentId) { // si ya estaba seteado previamente, agarro esa planta
                    $rootScope.currentId = 0
                }
                $rootScope.plant = $rootScope.data.plantas[$rootScope.currentId];
                $rootScope.collections = {};
                $scope.loading = false;
                $rootScope.updateHome = false; // flag de actualizar en false: no se volverá a pedir la data hasta no setearlo en true
            };
            var getDataFail = function(data){
                // console.log(data);
                $scope.loading = false;
                $scope.disconnect = true;
            };
            console.log

            $scope.loading = true;
            $scope.disconnect = false;
            if(AuthSrv.currentUser().type === 'contratista'){
                ApiHttpSrv.createApiHttp('post', ConfigSrv.getApiUrl('home'), data, data).success(getDataSuccess).error(getDataFail);
            }else{
                ApiHttpSrv.createApiHttp('post', ConfigSrv.getApiUrl('homePlanta'), data, data).success(getDataSuccess).error(getDataFail);
            }
        }
        if (AuthSrv.initialState() || !AuthSrv.authorized()) {
            $location.path('/login');
        }

        // si ya estaba seteada la planta no la actualizo (salvo que esté seteado el flag de actualizar)
        if (!$rootScope.plant || $rootScope.updateHome) {
            getdata();
        }
        $scope.update = function(){
            getdata();
        }

}]);
'use strict';

angular.module('myApp.homePlanta', [])
.controller('homePlantaCtrl',
    ['$scope', '$rootScope', 'ApiHttpSrv', 'ConfigSrv', '$location', 'AuthSrv', 'RedirectSrv',
    function($scope, $rootScope, ApiHttpSrv, ConfigSrv, $location, AuthSrv, RedirectSrv) {

        var getdata = function () {
            var data = {
                'token': AuthSrv.currentUser().token,
                'idEmpresa': AuthSrv.currentUser().idEmpresa
            };
            var getDataSuccess = function(data){
            //    console.log(data[0].plantas);

                $rootScope.plantas = data[0].plantas;
                if(!$rootScope.currentId) { // si ya estaba seteado previamente, agarro esa planta
                    $rootScope.currentId = 0
                }
                
                $rootScope.data = data[0].plantas[$rootScope.currentId];
                $rootScope.collections = {};
                $scope.loading = false;
                $rootScope.updateHome = false; // flag de actualizar en false: no se volverá a pedir la data hasta no setearlo en true
            };
            var getDataFail = function(data){
                // console.log(data);
                $scope.loading = false;
                $scope.disconnect = true;
            };
            console.log

            $scope.loading = true;
            $scope.disconnect = false;
            ApiHttpSrv.createApiHttp('post', ConfigSrv.getApiUrl('homePlanta'), data, data).success(getDataSuccess).error(getDataFail);
        }
        if (AuthSrv.initialState() || !AuthSrv.authorized()) {
            $location.path('/login');
        }

        // si ya estaba seteada la planta no la actualizo (salvo que esté seteado el flag de actualizar)
        if (!$rootScope.plant || $rootScope.updateHome) {
            getdata();
        }
        $scope.update = function(){
            getdata();
        }

}]);
'use strict';

angular.module('myApp.login', []).controller('loginCtrl', ['$scope', 'ApiHttpSrv', 'ConfigSrv', '$rootScope', 'AuthSrv', '$location',function($scope, ApiHttpSrv, ConfigSrv, $rootScope, AuthSrv, $location) {

    if (AuthSrv.authorized() && !AuthSrv.initialState()) {
       $location.path("/home");
    }
    $rootScope.loginfail = false;
    $rootScope.disconnect = false;

    $scope.login = function (user) {
        var data = {
            "user": $scope.user.name,
            "pass": $scope.user.password
        };
        AuthSrv.login(data);

    }

}]);
'use strict';

angular.module('myApp.reportes', [])
.controller('reportesCtrl',
    ['$scope', '$rootScope', '$window', 'AuthSrv', 'RedirectSrv', 'ApiHttpSrv', 'ConfigSrv',
    function($scope, $rootScope, $window, AuthSrv, RedirectSrv, ApiHttpSrv, ConfigSrv) {

    var url_get = ConfigSrv.getApiUrl('reportes');

    var getData = function () {
        var data = {
            'token': AuthSrv.currentUser().token,
            'idPlanta': $rootScope.plant.id,
        };
        var getDataSuccess = function(d){
            $scope.reportesData = {};
            angular.forEach(d,function(reporte) {
                if (!$scope.reportesData[reporte.Grupo]) {
                    $scope.reportesData[reporte.Grupo] = []
                };
                $scope.reportesData[reporte.Grupo].push({
                    'Nombre' : reporte.Nombre,
                    'Link' : reporte.Link
                });
            });
            $scope.loading = false;
        };
        var getDataFail = function(d){
            $scope.loading = false;
        };
        $scope.loading = true;
        ApiHttpSrv.createApiHttp('post', url_get, data, data).success(getDataSuccess).error(getDataFail);
    }

    if (AuthSrv.initialState() || !AuthSrv.authorized()) {
        RedirectSrv.redirect('/login');
    }else{
        getData();
    }

    $scope.openPDF = function(link){
        $window.open(link, '_system');
    }

}]);