"use strict";

 angular.module("AppConfig", [])

.constant("config", {
        "TAG":"Production Configuration File",
        "mocks" : {
          "menu" : {
              "enable":true
          }
        },
        "api":{
            "methods":{
                "login": "http://certronicweb.com/portal/appMovil/login.php",
                "logout": "data/mocks/logout/logout.json",
                "home": "http://certronicweb.com/portal/appMovil/contratista/home.php",
                "contratos": "http://certronicweb.com/portal/appMovil/contratista/tablaContratos.php",
                "contrato" : "http://certronicweb.com/portal/appMovil/contratista/datosContrato.php",
                "contratistas": "http://certronicweb.com/portal/appMovil/contratista/tablaContratistas.php",
                "contratista": "http://certronicweb.com/portal/appMovil/contratista/datosContratista.php",
                "personal": "http://certronicweb.com/portal/appMovil/contratista/tablaEmpleados.php",
                "empleado": "http://certronicweb.com/portal/appMovil/contratista/datosEmpleado.php",
                "vehiculos": "http://certronicweb.com/portal/appMovil/contratista/tablaVehiculos.php",
                "vehiculo": "http://certronicweb.com/portal/appMovil/contratista/datosVehiculo.php",
                "maquinarias": "http://certronicweb.com/portal/appMovil/contratista/tablaMaquinarias.php",
                "maquinaria": "http://certronicweb.com/portal/appMovil/contratista/datosMaquinaria.php",
                "reportes": "http://certronicweb.com/portal/appMovil/contratista/reportes.php",

                "homePlanta": "http://certronicweb.com/portal/appMovil/planta/homePlanta.php",
                "contratosPlanta": "http://certronicweb.com/portal/appMovil/planta/tablaContratos.php",
                "contratoPlanta" : "http://certronicweb.com/portal/appMovil/planta/datosContrato.php",
                "contratistasPlanta": "http://certronicweb.com/portal/appMovil/planta/tablaContratistas.php",
                "contratistaPlanta": "http://certronicweb.com/portal/appMovil/planta/datosContratista.php",
                "personalPlanta": "http://certronicweb.com/portal/appMovil/planta/tablaEmpleados.php",
                "empleadoPlanta": "http://certronicweb.com/portal/appMovil/planta/datosEmpleado.php",
                "vehiculosPlanta": "http://certronicweb.com/portal/appMovil/planta/tablaVehiculos.php",
                "vehiculoPlanta": "http://certronicweb.com/portal/appMovil/planta/datosVehiculo.php",
                "maquinariasPlanta": "http://certronicweb.com/portal/appMovil/planta/tablaMaquinarias.php",
                "maquinariaPlanta": "http://certronicweb.com/portal/appMovil/planta/datosMaquinaria.php",
                "excepcionesPlanta": "http://certronicweb.com/portal/appMovil/planta/tablaExcepciones.php",
                "excepcionPlanta": "http://certronicweb.com/portal/appMovil/planta/datosExcepcion.php"
                
            }
        }
    }
);